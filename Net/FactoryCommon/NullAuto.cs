namespace FactoryCommon
{
    public class NullAuto : IAuto
    {
        public string Name
        {
            get { return string.Empty; }
        }

        public void TurnOn()
        {
            
        }

        public void TurnOff()
        {
            
        }
    }
}