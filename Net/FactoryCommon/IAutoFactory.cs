﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryCommon
{
    public interface IAutoFactory
    {
        IAuto CreateSportsCar();
        IAuto CreateLuxuryCar();
        IAuto CreateEconomyCar();
    }
}
