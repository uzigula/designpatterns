using System;
using System.Collections.Generic;
using System.Linq;

namespace BeforeMediator
{
    public abstract class Aircraft
    {
        private int _currentAltitude;
        protected IList<Aircraft> _acknowledgedAircraft = new List<Aircraft>();

        protected Aircraft(string callSign)
        {
            CallSign = callSign;
        }

        public void Acknowledges(Aircraft aircraft)
        {
            _acknowledgedAircraft.Add(aircraft);
        }

        public abstract int Ceiling { get; }

        public abstract bool IsTrailingGapAcceptable();

        public string CallSign { get; private set; }

        public int Altitude
        {
            get { return _currentAltitude; }
            set
            {
                _currentAltitude = value;
                NoticeAcknowledgeAirCrafts();
            }
        }

        private void NoticeAcknowledgeAirCrafts()
        {
            foreach (var currentAircraftUnderGuidance in _acknowledgedAircraft)
            {
                if (Math.Abs(currentAircraftUnderGuidance.Altitude - Altitude) < 1000)
                {
                    Climb(1000);
                    currentAircraftUnderGuidance.WarnOfAirspaceIntrusionBy(this);
                }
            }
        }

        public void Climb(int heightToClimb)
        {
            Console.WriteLine($"{CallSign} climbig {heightToClimb} feets");
            Altitude += heightToClimb;
        }

        public void WarnOfAirspaceIntrusionBy(Aircraft reportingAircraft)
        {
            Console.WriteLine($"To {CallSign} => warning!! {reportingAircraft.CallSign} is closing dangerously");
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType()) return false;

            var incoming = (Aircraft) obj;
            return this.CallSign.Equals(incoming.CallSign);
        }
    }
}