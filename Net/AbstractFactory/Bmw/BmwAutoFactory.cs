﻿using FactoryCommon;

namespace AbstractFactory.Bmw
{
    public class BmwAutoFactory : IAutoFactory
    {
        public IAuto CreateSportsCar()
        {
            return new BMW335Xi();
        }

        public IAuto CreateLuxuryCar()
        {
            return new NullAuto();
        }

        public IAuto CreateEconomyCar()
        {
            return new NullAuto();
        }
    }
}
