﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactoryCommon;

namespace AbstractFactory.Mini
{
    public class MiniAutoFactory : IAutoFactory
    {
        public IAuto CreateSportsCar()
        {
            var mini = new MiniCooper();

            mini.AddSportPackage();

            return mini;
        }

        public IAuto CreateLuxuryCar()
        {
            var mini = new MiniCooper();

            mini.AddLuxuryPackage();

            return mini;
        }

        public IAuto CreateEconomyCar()
        {
            return new MiniCooper();
        }
    }
}
