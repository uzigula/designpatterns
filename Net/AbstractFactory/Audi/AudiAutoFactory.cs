﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactoryCommon;

namespace AbstractFactory.Audi
{
    public class AudiAutoFactory : IAutoFactory
    {
        public IAuto CreateSportsCar()
        {
            return new AudiR8();
        }

        public IAuto CreateLuxuryCar()
        {
            return new NullAuto();
        }

        public IAuto CreateEconomyCar()
        {
            return new AudiTTS();
        }
    }
}
