using System;
using FactoryCommon;

namespace AbstractFactory.Audi
{
    public class AudiR8 : IAuto
    {
        public string Name
        {
            get { return "Audi R8"; }
        }

        public void TurnOn()
        {
            Console.WriteLine("The Audi R8 is running, 0 to 100 kmh in 5 seconds!!!");
        }

        public void TurnOff()
        {
            Console.WriteLine("The Audi R8 is off");
        }
    }
}