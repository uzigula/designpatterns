﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactoryCommon;

namespace AbstractFactory
{
    class Program
    {
        static void Main(string[] args)
        {
            Factory factories = new Factory();
            IAutoFactory autoFactory = factories.CreateInstance(args[0]);

            IAuto car = autoFactory.CreateSportsCar();

            car.TurnOn();
            car.TurnOff();

        }
        //static IAutoFactory LoadFactory()
        //{
        //    string factoryName = Properties.Settings.Default.AutoFactory;
        //    return Assembly.GetExecutingAssembly().CreateInstance(factoryName) as IAutoFactory;
        //}
    }
}
