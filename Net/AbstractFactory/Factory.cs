﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using FactoryCommon;

namespace AbstractFactory
{
    public class Factory
    {
        Dictionary<string, Type> factories;

        public Factory()
        {
            LoadTypesICanReturn();
        }

        public IAutoFactory CreateInstance(string brand)
        {
            Type t = GetTypeToCreate(brand);

            if (t == null)
                return null;

            return Activator.CreateInstance(t) as IAutoFactory;
        }

        Type GetTypeToCreate(string brand)
        {
            return (from auto in factories
                    where auto.Key.Contains(brand)
                    select factories[auto.Key]).FirstOrDefault();
        }

        void LoadTypesICanReturn()
        {
            factories = new Dictionary<string, Type>();

            Type[] typesInThisAssembly = Assembly.GetExecutingAssembly().GetTypes();

            foreach (Type type in typesInThisAssembly)
            {
                if (type.GetInterface(typeof(IAutoFactory).ToString()) != null)
                {
                    factories.Add(type.Name.ToLower(), type);
                }
            }
        }
    }
}
