﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator
{
    class Program
    {
        static void Main(string[] args)
        {
            var yytCenter = new LimaAiport();

            var flight1 = new Airbus307("LA2591", yytCenter);
            var flight2 = new Boeing737200("WS203", yytCenter);
            var flight3 = new Embraer190("AC602", yytCenter);

            flight1.Altitude += 800;
            flight3.Altitude += 500;
            Console.ReadKey();
        }
    }
}
