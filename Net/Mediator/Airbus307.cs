namespace Mediator
{
    public class Airbus307 : Aircraft
    {
        public Airbus307(string callSign, IAirTrafficControl atc) : base(callSign, atc)
        {
        }

        public override int Ceiling
        {
            get { return 41000; }
        }
    }
}