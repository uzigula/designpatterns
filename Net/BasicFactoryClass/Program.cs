﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactoryCommon;
using SimpleFactory.Autos;

namespace BasicFactoryClass
{
    class Program
    {
        static void Main(string[] args)
        {
            string carName = args[0];

            IAuto car = new AutoFactory().CreateInstance(carName);
            car.TurnOn();
            car.TurnOff();
        }
    }
}
