﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactoryCommon;
using SimpleFactory.Autos;

namespace BasicFactoryClass.Autos
{
    public class SubaruXV : IAuto
    {
        public string Name => "Subaru XV 2.0 MT";
        public void TurnOn()
        {
            Console.WriteLine("This subaru is ready for out road");
        }

        public void TurnOff()
        {
            Console.WriteLine("the happy time is over");
        }
    }
}
