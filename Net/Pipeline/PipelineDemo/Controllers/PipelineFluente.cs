﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PipelineDemo.Controllers
{
    public interface IRouteFilterDataProvider
    {
        bool DebtorIsAnAdversaryProceeding(string projectCode, long debtorId);
    }

    internal sealed class RouteInfo
    {
        public RouteInfo(string originUrl)
        {
            RedirectPathAndQuery = originUrl;
            OriginUrl = originUrl;
            RedirectToUrl = GetWebUrlFromComfig(); // allways at the begining point to Website
        }

        private string GetWebUrlFromComfig()
        {
            string value = ConfigurationManager.AppSettings["WebUrl"];
            return string.IsNullOrEmpty(value) ? string.Empty : value;
        }

        public RouteInfo(string originUrl, string redirectToUrl, string redirectPathAndQuery)
        {
            RedirectPathAndQuery = redirectPathAndQuery;
            OriginUrl = originUrl;
            RedirectToUrl = string.IsNullOrEmpty(redirectToUrl) ? GetWebUrlFromComfig() : redirectToUrl;
        }
        public string RedirectToUrl { get; private set; }
        public string OriginUrl { get; private set; }
        public string RedirectPathAndQuery { get; private set; }

        public bool Changed => !String.Equals(OriginUrl, RedirectPathAndQuery, StringComparison.InvariantCultureIgnoreCase);

        public bool IsRedirectingToWebUrl => String.Equals(RedirectToUrl, GetWebUrlFromComfig(), StringComparison.InvariantCultureIgnoreCase);

    }
    internal interface IRouteFilter
    {
        RouteInfo Execute(RouteInfo input);
        void Register(IRouteFilter filter);
    }

    internal sealed class RoutePipeline
    {
        private readonly IRouteFilterDataProvider dataProvider;
        private IRouteFilter _rootFilter;
        public RoutePipeline(IRouteFilterDataProvider provider)
        {
            dataProvider = provider;
        }

        public RoutePipeline Then(IRouteFilter filter)
        {
            if (_rootFilter == null) _rootFilter = filter;
            else _rootFilter.Register(filter);

            return this;
        }

        public RouteInfo Process(RouteInfo input)
        {
            if (_rootFilter == null) return input;

            return _rootFilter.Execute(input);
        }

        public RoutePipeline AutoLoadFilters()
        {
            var types = Assembly
                          .GetExecutingAssembly()
                          .GetTypes()
                          .Where(x => x.IsClass && !x.IsAbstract && x.IsSubclassOf(typeof(BaseRouteFilter))).ToList();
            types.ForEach(t => Add((IRouteFilter)Activator.CreateInstance(t, dataProvider)));
            return this;
        }

    }

    // la base con la que yo debo construir todos los filtros
    internal abstract class BaseRouteFilter : IRouteFilter
    {
        protected abstract RouteInfo Process(RouteInfo input);
        protected readonly string webUrl;
        protected readonly string documentApi;
        protected readonly string adminUrl;
        protected readonly IRouteFilterDataProvider dataProvider;
        protected readonly string defaultWebLandingUrl;
        private IRouteFilter _nextFilter;

        public BaseRouteFilter(IRouteFilterDataProvider provider)
        {
            webUrl = ConfigurationManager.AppSettings["WebUrl"];
            documentApi = ConfigurationManager.AppSettings["DocumentServer"];
            adminUrl = ConfigurationManager.AppSettings["adminUrl"];
            dataProvider = provider;
        }

        public RouteInfo Execute(RouteInfo input)
        {
            RouteInfo result = Process(input);
            if (_nextFilter != null ) result = _nextFilter.Execute(result);
            return result;

        }

        public void Register(IRouteFilter filter)
        {

            if (_nextFilter == null) _nextFilter = filter;
            else _nextFilter.Register(filter);

        }
    }

    internal sealed class GoDocketRouteFilter : BaseRouteFilter
    {
        private IRouteFilterDataProvider dataprovider;

        public GoDocketRouteFilter(IRouteFilterDataProvider provider):base(provider)
        {
        }

        protected override RouteInfo Process(RouteInfo input)
        {
            //url handled: http://dm.acme.com/TBC/Docket?Debtors=5603&RelatedDocketId=&ds=true&maxPerPage=25&page=1
            var exp = @"^\/([A-Z0-9]{3,5})\/[dD]ocket\?[dD]ebtors\=([0-9]*)\&[\w\W]*$";
            Match match = Regex.Match(input.OriginUrl, exp, RegexOptions.IgnoreCase); ;

            if (match.Success)
            {
                string route = "dockets";
                string projectCode = Convert.ToString(match.Groups[1].Value);
                long debtorId = Convert.ToInt64(match.Groups[2].Value);
                try
                {
                    if (dataProvider.DebtorIsAnAdversaryProceeding(projectCode, debtorId)) route = "advProc";

                    return new RouteInfo(input.OriginUrl,
                        $"{webUrl}/case/{projectCode}/{route}/?debtorId={debtorId}",
                        $"/case/{projectCode}/{route}/?debtorId={debtorId}");
                }
                catch
                {
                    return new RouteInfo(input.OriginUrl, $"{webUrl}/case/{projectCode}/{route}", $"/case/{projectCode}/{route}");
                }
            }
            return input;
        }
    }
}
