﻿using System;
using System.Configuration;
using System.Text.RegularExpressions;

namespace PipelineDemo.Controllers
{
    internal class CaseProvider
    {
        public static string GetDefaultPage(string projectCode)
        {
            return "Case";
        }
    }


    internal sealed class RouteInfo
    {
        public RouteInfo(string originUrl)
        {
            RedirectPathAndQuery = originUrl;
            OriginUrl = originUrl;
            RedirectToUrl = GetWebUrlFromComfig(); // allways at the begining point to Website
        }

        private string GetWebUrlFromComfig()
        {
            string value = ConfigurationManager.AppSettings["WebUrl"];
            return string.IsNullOrEmpty(value) ? string.Empty : value;
        }

        public RouteInfo(string originUrl, string redirectToUrl, string redirectPathAndQuery)
        {
            RedirectPathAndQuery = redirectPathAndQuery;
            OriginUrl = originUrl;
            RedirectToUrl = string.IsNullOrEmpty(redirectToUrl) ? GetWebUrlFromComfig() : redirectToUrl;
        }
        public string RedirectToUrl { get; private set; }
        public string OriginUrl { get; private set; }
        public string RedirectPathAndQuery { get; private set; }

        public bool Changed => !String.Equals(OriginUrl, RedirectPathAndQuery, StringComparison.InvariantCultureIgnoreCase);

        public bool IsRedirectingToWebUrl => String.Equals(RedirectToUrl, GetWebUrlFromComfig(), StringComparison.InvariantCultureIgnoreCase);

    }

    internal interface IRouteFilter
    {
        RouteInfo Apply(RouteInfo input);
    }

    internal sealed class GoDocketRouteFilter : IRouteFilter
    {
        private IRouteFilterDataProvider dataProvider;

        public GoDocketRouteFilter(IRouteFilterDataProvider provider)
        {
            dataProvider = provider;
        }

        public RouteInfo Apply(RouteInfo input)
        {
            //url handled: http://dm.acme.com/TBC/Docket?Debtors=5603&RelatedDocketId=&ds=true&maxPerPage=25&page=1
            var exp = @"^\/([A-Z0-9]{3,5})\/[dD]ocket\?[dD]ebtors\=([0-9]*)\&[\w\W]*$";
            Match match = Regex.Match(input.OriginUrl, exp, RegexOptions.IgnoreCase); ;

            if (match.Success)
            {
                string route = "dockets";
                string projectCode = Convert.ToString(match.Groups[1].Value);
                long debtorId = Convert.ToInt64(match.Groups[2].Value);
                string webUrl=string.Empty;
                try
                {
                    if (dataProvider.DebtorIsAnAdversaryProceeding(projectCode, debtorId)) route = "advProc";

                    return new RouteInfo(input.OriginUrl,
                        $"{webUrl}/case/{projectCode}/{route}/?debtorId={debtorId}",
                        $"/case/{projectCode}/{route}/?debtorId={debtorId}");
                }
                catch
                {
                    return new RouteInfo(input.OriginUrl, $"{webUrl}/case/{projectCode}/{route}", $"/case/{projectCode}/{route}");
                }
            }
            return input;
        }
    }

    public interface IRouteFilterDataProvider
    {
        bool DebtorIsAnAdversaryProceeding(string projectCode, long debtorId);
    }

}