﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PipelineDemo.Controllers
{
    public class RedirectController : Controller
    {
        private IRouteFilterDataProvider dataProvider;

        public RedirectController(IRouteFilterDataProvider provider)
        {
            dataProvider = provider;
        }
        public ActionResult Index()
        {
            RouteInfo newRoute = new RoutePipeline(dataProvider)
                .AutoLoadFilters()
                .Process(new RouteInfo(Request.Url.PathAndQuery));

            if (newRoute.IsRedirectingToWebUrl) return View();
            return Redirect(Url.Content(newRoute.RedirectToUrl));
            
        }
    }



}
