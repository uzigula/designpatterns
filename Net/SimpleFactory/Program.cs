﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactoryCommon;
using SimpleFactory.Autos;

namespace SimpleFactory
{
    class Program
    {
        static void Main(string[] args)
        {
            string carName = args[0];

            IAuto car = AutoFactory.GetCar(carName);
            car.TurnOn();
            car.TurnOff();
        }
    }
}
