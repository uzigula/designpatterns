﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactoryCommon;

namespace SimpleFactory.Autos
{
    internal class AutoFactory
    {
        public static IAuto GetCar(string carName)
        {
            switch (carName)
            {
                case "bmw":
                    return new BMW335Xi();
                case "mini":
                    return new MiniCooper();
                case "audi":
                    return new AudiTTS();
                default:
                    return new NullAuto();
            }
        }
    }
}
