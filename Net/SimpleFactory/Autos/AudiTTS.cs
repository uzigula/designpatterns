using System;
using FactoryCommon;

namespace SimpleFactory.Autos
{
    public class AudiTTS : IAuto
    {
        public string Name
        {
            get { return "Audi TTS"; }
        }

        public void TurnOn()
        {
            Console.WriteLine("The Audi TTS is running, but the battery is dry");
        }

        public void TurnOff()
        {
            Console.WriteLine("The Audi TTS is off, please plugin to charge the battery.");
        }
    }
}