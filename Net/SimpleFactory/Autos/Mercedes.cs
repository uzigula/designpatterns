﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactoryCommon;

namespace SimpleFactory.Autos
{
    public class Mercedes : IAuto
    {
        public string Name { get; }
        public void TurnOn()
        {
            Console.WriteLine("otra meche corriendo");
        }

        public void TurnOff()
        {
            Console.WriteLine("otra meche que se apago");
        }
    }
}
